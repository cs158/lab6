namespace VrijemeHR {
    public partial class Form1 : Form {
        public Form1() {
            InitializeComponent();

            vrijeme = new Vrijeme();
            OsvjeziGradove();
            OsvjeziNajhladnijeINajtoplijeGradove();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e) {


        }
        private void OsvjeziGradove() {
            CbGradovi.Items.Clear();
            foreach(var grad in vrijeme.DohvatiGradove()) {
                CbGradovi.Items.Add(grad);
            }

        }
     


        private Vrijeme vrijeme;

        private void Form1_Load(object sender, EventArgs e) {

        }

        private void LbGrad_Click(object sender, EventArgs e) {

        }
        private void PrikaziPodatkeZaGrad(string grad)
        {
            var podaci = vrijeme.DohvatiPodatkeZaGrad(grad);

            LbGrad.Text = podaci.Grad;
            LbTemp.Text = podaci.TemperaturaZaPrikaz;
            LbVlaga.Text = podaci.VlagaZaPrikaz;
            LbTlak.Text = podaci.TlakZaPrikaz;
        }

        private void CbGradovi_SelectedIndexChanged_1(object sender, EventArgs e) {
            PrikaziPodatkeZaGrad(CbGradovi.Text);
        }

        private void OsvjeziNajhladnijeINajtoplijeGradove()
        {
            lbNajtopliji.Items.Clear();
            lbNajhladniji.Items.Clear();

            lbNajtopliji.Items.AddRange(vrijeme.DohvatiNajtoplijeGradove(5).ToArray());
            lbNajhladniji.Items.AddRange(vrijeme.DohvatiNajhladnijeGradove(5).ToArray());
        }


        private void label1_Click(object sender, EventArgs e) {

        }

        private void LbVlaga_Click(object sender, EventArgs e)
        {

        }

        private void lbNajtopliji_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }
    }
}