﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace VrijemeHR {
   public class Vrijeme {
        public Vrijeme() {
            vrijeme = new XmlDocument();
            vrijeme.Load("http://vrijeme.hr/hrvatska_n.xml");
        }

        public List<VrijemePodatak> DohvatiPodatke() {
            List<VrijemePodatak> podaci = new List<VrijemePodatak>();

            XmlNodeList gradovi = vrijeme.GetElementsByTagName("Grad");
            foreach (XmlNode grad in gradovi) {
                podaci.Add(new VrijemePodatak(
                    grad["GradIme"].InnerText,
                    grad["Podatci"]["Temp"].InnerText,
                    grad["Podatci"]["Vlaga"].InnerText,
                    grad["Podatci"]["Tlak"].InnerText
                ));
            }
            return podaci;
        }

        public IEnumerable<string> DohvatiGradove() {
            var podaci = DohvatiPodatke();

            var gradovi =
                from p in podaci
                orderby p.Grad
                select p.Grad;

            return gradovi;
        }

        public VrijemePodatak DohvatiPodatkeZaGrad(string naziv) {
            var podaci = DohvatiPodatke();

            var grad =
                (from p in podaci
                where p.Grad == naziv
                orderby p.Grad
                select p).First();

            return grad;
        }

        public IEnumerable<VrijemePodatak> DohvatiNajtoplijeGradove(int kolicina)
        {
            var podaci = DohvatiPodatke();
            return
              (from p in podaci
               orderby p.Temperatura descending
               select p).Take(kolicina);
        }

        public IEnumerable<VrijemePodatak> DohvatiNajhladnijeGradove(int kolicina)
        {
            var podaci = DohvatiPodatke();
            return

              (from p in podaci
               orderby p.Temperatura
               select p).Take(kolicina);
        }

        private XmlDocument vrijeme;
    }

}
