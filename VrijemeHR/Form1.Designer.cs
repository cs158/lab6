﻿namespace VrijemeHR {
    partial class Form1 {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.CbGradovi = new System.Windows.Forms.ComboBox();
            this.LbGrad = new System.Windows.Forms.Label();
            this.LbTemp = new System.Windows.Forms.Label();
            this.LbVlaga = new System.Windows.Forms.Label();
            this.LbTlak = new System.Windows.Forms.Label();
            this.lbNajhladniji = new System.Windows.Forms.ListBox();
            this.lbNajtopliji = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // CbGradovi
            // 
            this.CbGradovi.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.CbGradovi.FormattingEnabled = true;
            this.CbGradovi.Location = new System.Drawing.Point(375, 16);
            this.CbGradovi.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.CbGradovi.Name = "CbGradovi";
            this.CbGradovi.Size = new System.Drawing.Size(138, 28);
            this.CbGradovi.TabIndex = 0;
            this.CbGradovi.SelectedIndexChanged += new System.EventHandler(this.CbGradovi_SelectedIndexChanged_1);
            // 
            // LbGrad
            // 
            this.LbGrad.Location = new System.Drawing.Point(375, 97);
            this.LbGrad.Name = "LbGrad";
            this.LbGrad.Size = new System.Drawing.Size(138, 37);
            this.LbGrad.TabIndex = 1;
            this.LbGrad.Text = "(grad nije odabran)";
            this.LbGrad.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.LbGrad.Click += new System.EventHandler(this.LbGrad_Click);
            // 
            // LbTemp
            // 
            this.LbTemp.Location = new System.Drawing.Point(375, 177);
            this.LbTemp.Name = "LbTemp";
            this.LbTemp.Size = new System.Drawing.Size(114, 35);
            this.LbTemp.TabIndex = 2;
            this.LbTemp.Text = "-";
            this.LbTemp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.LbTemp.Click += new System.EventHandler(this.label1_Click);
            // 
            // LbVlaga
            // 
            this.LbVlaga.Location = new System.Drawing.Point(375, 274);
            this.LbVlaga.Name = "LbVlaga";
            this.LbVlaga.Size = new System.Drawing.Size(114, 31);
            this.LbVlaga.TabIndex = 3;
            this.LbVlaga.Text = "-";
            this.LbVlaga.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.LbVlaga.Click += new System.EventHandler(this.LbVlaga_Click);
            // 
            // LbTlak
            // 
            this.LbTlak.Location = new System.Drawing.Point(375, 357);
            this.LbTlak.Name = "LbTlak";
            this.LbTlak.Size = new System.Drawing.Size(114, 31);
            this.LbTlak.TabIndex = 4;
            this.LbTlak.Text = "-";
            this.LbTlak.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbNajhladniji
            // 
            this.lbNajhladniji.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lbNajhladniji.FormattingEnabled = true;
            this.lbNajhladniji.ItemHeight = 20;
            this.lbNajhladniji.Location = new System.Drawing.Point(593, 284);
            this.lbNajhladniji.Name = "lbNajhladniji";
            this.lbNajhladniji.Size = new System.Drawing.Size(240, 244);
            this.lbNajhladniji.TabIndex = 5;
            // 
            // lbNajtopliji
            // 
            this.lbNajtopliji.BackColor = System.Drawing.SystemColors.Info;
            this.lbNajtopliji.FormattingEnabled = true;
            this.lbNajtopliji.ItemHeight = 20;
            this.lbNajtopliji.Location = new System.Drawing.Point(70, 284);
            this.lbNajtopliji.Name = "lbNajtopliji";
            this.lbNajtopliji.Size = new System.Drawing.Size(240, 244);
            this.lbNajtopliji.TabIndex = 6;
            this.lbNajtopliji.SelectedIndexChanged += new System.EventHandler(this.lbNajtopliji_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(117, 250);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 20);
            this.label1.TabIndex = 7;
            this.label1.Text = "Najtopliji gradovi";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(641, 250);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(136, 20);
            this.label2.TabIndex = 8;
            this.label2.Text = "Najhladniji gradovi";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(914, 600);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbNajtopliji);
            this.Controls.Add(this.lbNajhladniji);
            this.Controls.Add(this.LbTlak);
            this.Controls.Add(this.LbVlaga);
            this.Controls.Add(this.LbTemp);
            this.Controls.Add(this.LbGrad);
            this.Controls.Add(this.CbGradovi);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Form1";
            this.Text = "Vrijeme";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ComboBox CbGradovi;
        private Label LbGrad;
        private Label LbTemp;
        private Label LbVlaga;
        private Label LbTlak;
        private ListBox lbNajhladniji;
        private ListBox lbNajtopliji;
        private Label label1;
        private Label label2;
    }
}